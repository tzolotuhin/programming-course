cmake_minimum_required(VERSION 3.10)
project(fib C)

set(CMAKE_C_STANDARD 99)

add_executable(fib main.c)