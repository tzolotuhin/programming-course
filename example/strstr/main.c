#include <stdio.h>

int main(int argc, char** argv)
{
	char str[] = "Hello world";
	char sub_str[] = "ello";

	int index = -1;
	for (int i = 0; str[i] != 0; i++) {
		int check = 0;
		for (int j = 0; sub_str[j] != 0 && str[i + j] != 0; j++) {
			if (sub_str[j] != str[i + j]) {
				check = 1;
				break;
			}
		}

		if (!check) {
			index = i;
			break;
		}
	}

	// output
	printf("index: %d\n", index);

	return 0;
}