#include <stdio.h>

int fibo(int n) {
	if (n < 1)
		return 0;
	if (n < 3)
		return 1;
	return fibo(n - 1) + fibo(n - 2);
}

int main(int argc, char** argv) {

	printf("Fibo: ");
	for (int i = 0; i < 10; i++) {
		printf("%d ", fibo(i));
	}
	printf("\n");

	return 0;
}